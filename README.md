# orocms-plugin-markdown

Markdown plugin for the [OroCMS Admin](https://gitlab.com/rudenyl/orocms-admin.git) [Article module](https://gitlab.com/rudenyl/orocms-module-articles.git).
